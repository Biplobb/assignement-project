package com.babl.Assignment.modules.login.Repositories;

import com.babl.Assignment.modules.login.Schemas.User;
 import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    //List<User> findAll
    User findByUsername(String username);


}
