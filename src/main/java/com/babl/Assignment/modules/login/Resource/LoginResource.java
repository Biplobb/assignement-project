package com.babl.Assignment.modules.login.Resource;

import lombok.Data;

@Data
public class LoginResource {
    private String accessToken;
    private Object response;
    private int status;
}
