package com.babl.Assignment.modules.login;

import com.babl.Assignment.common.Constants;
import com.babl.Assignment.configs.BearerTokenValidationFilter;
import com.babl.Assignment.datasources.oracle.logins.BearerToken;
import com.babl.Assignment.datasources.oracle.logins.BearerTokenRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenService {
    private final BearerTokenRepository bearerTokenRepository;

    public String generateUpdateToken(String username, Optional<BearerToken> bearerToken) {
        byte[] key = Constants.tokenSecretKey.getBytes();

        String jwtString = Jwts.builder().setIssuer(Constants.tokenIssuer)
                .setSubject(username)
                .setHeaderParam("random", new Random().nextInt())
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();

        BearerToken token;
        if (!bearerToken.isPresent())
            token = new BearerToken();
        else
            token = bearerToken.get();


        token.setId(jwtString);
        token.setToken(jwtString);
        token.setUsername(username);
        token.setTimeout(LocalDateTime.now().plusMinutes(Constants.sessionTimeoutMinute));
        bearerTokenRepository.save(token);

        return jwtString;
    }

    public boolean isDuplicateActiveToken(Optional<BearerToken> bearerToken) {
        if (!bearerToken.isPresent())
            return false;
        LocalDateTime timeout = bearerToken.get().getTimeout();

        return timeout.isAfter(LocalDateTime.now());
    }

    public String getName(HttpServletRequest request) {
        String token = BearerTokenValidationFilter.resolveFromAuthorizationHeader(request);
        if (token == null)
            return null;

        byte[] key = Constants.tokenSecretKey.getBytes();
        Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        return claims.getBody().getSubject();
    }

    public BearerToken getToken(HttpServletRequest request) {
        String token = BearerTokenValidationFilter.resolveFromAuthorizationHeader(request);
        Optional<BearerToken> bearerTokenOptional = bearerTokenRepository.findById(token);
        if (bearerTokenOptional.isPresent())
            return bearerTokenOptional.get();
        else
            return null;
    }
}
