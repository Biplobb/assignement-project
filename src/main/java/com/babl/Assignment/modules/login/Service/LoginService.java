package com.babl.Assignment.modules.login.Service;


import com.babl.Assignment.common.Constants;
import com.babl.Assignment.modules.login.Repositories.UserRepository;
import com.babl.Assignment.modules.login.Schemas.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

 import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Random;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginService {
    private final HttpServletResponse httpServletResponse;
    private final HttpServletRequest httpServletRequest;

     private final UserRepository userRepository;




    public String logMeIn(Map<String, String> userCredential) {
        String username = userCredential.get("username");
        String password = userCredential.get("password");
    
        User user = userRepository.findByUsername(username);

        if (user == null) {
            return "";
        }
        byte[] key = Constants.tokenSecretKey.getBytes();


        String jwtString = Jwts.builder().setIssuer(Constants.tokenIssuer)
                .setSubject(username)
                .setHeaderParam("random", new Random().nextInt())
                .signWith(SignatureAlgorithm.HS256, key)
                .setExpiration(Date.from(ZonedDateTime.now().plusMinutes(1).toInstant()))
                .compact();
        return jwtString;
     }

    ///@Scheduled(fixedDelay = 3600000, initialDelay = 600000)
//    @Scheduled(fixedDelay = 3600000)
/*    public void deleteTimeoutToken() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:YYYY hh:mm:ss");

        String queryString = "delete from bearer_token where timeout <= to_date('" +
                LocalDateTime.now().format(formatter) + "', 'DD:MM:YYYY HH24:MI:SS')";
        entityManager.createNativeQuery(queryString).executeUpdate();

}*/
}