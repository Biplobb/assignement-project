package com.babl.Assignment.modules.login.Controller;


import com.babl.Assignment.modules.login.Service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginController {
     private final LoginService loginService;

    @RequestMapping(value = "/logMeIn", method = RequestMethod.POST)
    public String login(@RequestBody Map<String, String> userCredential) {
        System.out.println("ok");
        return loginService.logMeIn(userCredential);
    }



}
