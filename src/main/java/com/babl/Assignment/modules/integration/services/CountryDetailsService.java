package com.babl.Assignment.modules.integration.services;

import com.babl.Assignment.common.Constants;

import com.babl.Assignment.modules.login.TokenService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Transactional
public class CountryDetailsService {

    private final TokenService tokenService;
    private final HttpServletRequest httpServletRequest;


    private HttpHeaders createHttpHeaders(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "bearer " + accessToken);
        return headers;
    }


    public Object exchangeRestTemplate(String apiUrl) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHttpHeaders(httpServletRequest.getHeader("Authorization"));
        ResponseEntity<String> response;
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity responseEntity = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
        return responseEntity;
    }

    public Object getCountryDetails(String countryName){
        String apiUrl = Constants.countryDetailsUrl+countryName;
        return  exchangeRestTemplate(apiUrl);
    }

    public Object getExchangeRate(String baseCurrency) {
        String apiUrl = Constants.currencyExchangeRateUrl + baseCurrency;
        return  exchangeRestTemplate(apiUrl);


    }


}
