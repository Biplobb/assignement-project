package com.babl.Assignment.modules.integration.controllers;


 import com.babl.Assignment.modules.integration.services.CountryDetailsService;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryDetailsController {
    private final CountryDetailsService countryDetailsService;
    @GetMapping("/getCountryDetails/{countryName}")
    public  Object getCountryDetails(@PathVariable("countryName") final String countryName) {
        return countryDetailsService.getCountryDetails(countryName);
    }
    @GetMapping("/getExchangeRate/{currency}")
    public  Object getExchangeRate(@PathVariable("currency") final String currency) {
        return countryDetailsService.getExchangeRate(currency);
    }




//stable 


}
