package com.babl.Assignment.datasources.oracle.logins;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "bearer_token")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BearerToken implements Serializable {
    @Id
    private String id;
    private String username;
    private String token;

    @Column(name = "timeout", columnDefinition = "TIMESTAMP")
    private LocalDateTime timeout;
}
