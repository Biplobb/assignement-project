package com.babl.Assignment.datasources.oracle.logins;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface BearerTokenRepository extends JpaRepository<BearerToken, String> {
    BearerToken findByToken(String token);

    List<BearerToken> findByUsername(String username);

    List<BearerToken> findByUsernameAndTimeoutGreaterThan(String username, LocalDateTime localDateTime);

    List<BearerToken> findByTimeoutGreaterThanOrderByTimeoutDesc(LocalDateTime localDateTime);
}
