package com.babl.Assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitybankApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitybankApplication.class, args);
	}

}
