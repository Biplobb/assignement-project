package com.babl.Assignment.exceptions;

public class VariableNotSetException extends RuntimeException {
    public VariableNotSetException(String msg) {
        super(msg);
    }
}
