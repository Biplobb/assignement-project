package com.babl.Assignment.exceptions;

public class FormatException extends RuntimeException {
    public FormatException(String msg) {
        super(msg);
    }
}
