package com.babl.Assignment.exceptions;

public class StartCaseException extends RuntimeException {
    public StartCaseException(String code) {
        super(code);
    }
}
