package com.babl.Assignment.exceptions;

public class WrongOptionException extends RuntimeException {
    public WrongOptionException(String msg) {
        super(msg);
    }
}
