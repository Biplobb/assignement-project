package com.babl.Assignment.exceptions;

public class UserAccessException extends RuntimeException {
    public UserAccessException(String code) {
        super(code);
    }
}
