package com.babl.Assignment.configs.notused;//package com.babl.citybank.configs;
//
//import com.babl.citybank.common.Constants;
//import com.babl.citybank.common.PasswordStatus;
//import com.babl.citybank.common.UserStatus;
//import com.babl.citybank.datasources.oracle.adminPanel.repositories.*;
//import com.babl.citybank.datasources.oracle.adminPanel.schemas.LoginAuditTrial;
//import com.babl.citybank.datasources.oracle.adminPanel.schemas.Role;
//import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
//import com.babl.citybank.datasources.oracle.adminPanel.schemas.UserRole;
//import com.babl.citybank.datasources.oracle.workflow.repositories.BranchMasterRepository;
//import com.babl.citybank.datasources.oracle.workflow.schemas.BranchMaster;
//import com.babl.citybank.modules.adminPanel.services.UserRoleService;
//import lombok.Data;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.session.SessionInformation;
//import org.springframework.security.core.session.SessionRegistry;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.time.LocalDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.*;
//
//@Slf4j
//@Component
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
//public class MyAuthProvider implements AuthenticationProvider {
//    private final HttpServletResponse httpServletResponse;
//    private final HttpServletRequest httpServletRequest;
//    private final UserRoleService userRoleService;
//    private final UserRepository userRepository;
//    private final UserRoleRepository userRoleRepository;
//    private final RoleRepository roleRepository;
//    private final DelegationRepository delegationRepository;
//    private final LeaveRepository leaveRepository;
//    private final SessionRegistry sessionRegistry;
//    private final PasswordEncoder passwordEncoder;
//    private final LoginAuditTrialRepository auditTrialRepository;
//
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        String username = authentication.getName();
//        String password = authentication.getCredentials().toString();
//
//        User user = userRepository.findByUsername(username);
//
//        if (user == null) {
//            httpServletResponse.setHeader("errorMsg", "Username or password invalid");
//            httpServletResponse.setHeader("errorName", "invalidCredential");
//            throw new BadCredentialsException("Username or password invalid");
//        }
//
//        if (user.getStatus().equals(UserStatus.LOCKED)){
//            httpServletResponse.setHeader("errorMsg", "Your Account is Locked.");
//            httpServletResponse.setHeader("errorName", "accountLocked");
//            throw new BadCredentialsException("Your Account Is Locked.");
//        }
//
//        if (user.getStatus().equals(UserStatus.DISABLED) || user.getStatus().equals(UserStatus.SYSTEM_DISABLED) || user.getStatus().equals(UserStatus.TEMPORARY_DISABLED)){
//            httpServletResponse.setHeader("errorMsg", "Your Account is Disabled.");
//            httpServletResponse.setHeader("errorName", "accountDisabled");
//            throw new BadCredentialsException("Your Account Is Disabled.");
//        }
//
//        if (!passwordEncoder.matches(password, user.getPassword())) {
//            int loginError = user.getLoginErrorNum() == null ? 0 : user.getLoginErrorNum();
//            user.setLoginErrorNum(loginError + 1);
//
//
//            if (loginError >= 2){
//                user.setStatus(UserStatus.LOCKED);
//                userRepository.save(user);
//                httpServletResponse.setHeader("errorMsg", "Your Account is Locked.");
//                httpServletResponse.setHeader("errorName", "invalidCredential");
//                throw new BadCredentialsException("Your Account Is Locked.");
//            }
//
//            userRepository.save(user);
//            httpServletResponse.setHeader("errorMsg", "Username or password invalid");
//            httpServletResponse.setHeader("errorName", "invalidCredential");
//            throw new BadCredentialsException("Username or password invalid");
//        }
//
//        List<SessionInformation> sessionList = sessionRegistry.getAllSessions(username, false);
//
//
//        if (sessionList.size() > 0){
//            httpServletResponse.setHeader("errorMsg", "User Already Logged In");
//            httpServletResponse.setHeader("errorName", "multipleSession");
//            throw new BadCredentialsException("User Already Logged In");
//        }
//
//
//        if (user.getLastLogin() != null) {
//            long daysBetweenLogin = ChronoUnit.DAYS.between(user.getLastLogin().toLocalDate(), LocalDateTime.now().toLocalDate());
//
//            if (daysBetweenLogin > Constants.userDisableDateCount) {
//                user.setStatus(UserStatus.DISABLED);
//                userRepository.save(user);
//
//                httpServletResponse.setHeader("errorMsg", "Your Account is Disabled");
//                httpServletResponse.setHeader("errorName", "accountDisabled");
//                throw new BadCredentialsException("Your Account is Disabled");
//            }
//        }
//
//        if (user.getLastPasswordChanged() != null) {
//            long passwordChangeDaysBetween = ChronoUnit.DAYS.between(user.getLastPasswordChanged().toLocalDate(), LocalDateTime.now().toLocalDate());
//            if (passwordChangeDaysBetween > Constants.passwordExpirationDateCount)
//                user.setPasswordStatus(PasswordStatus.EXPIRED);
//        }
//
//        user.setLastLogin(LocalDateTime.now());
//        user.setLoginErrorNum(0);
//        userRepository.save(user);
//
//
//        LoginAuditTrial auditTrial = new LoginAuditTrial();
//        auditTrial.setUsername(username);
//        if (user.getStatus() != null)
//            auditTrial.setUserStatus(user.getStatus().toString());
//        auditTrial.setRemoteIp(httpServletRequest.getRemoteAddr());
//        auditTrial.setPcHostName(httpServletRequest.getRemoteHost());
//        auditTrial.setOsUser(httpServletRequest.getRemoteUser());
//        auditTrial.setLoginAt(LocalDateTime.now());
//
//
//        UserRole userRole = userRoleRepository.findByUsername(username);
//        if (userRole != null) {
//            auditTrial.setWorkplace(userRole.getWorkplace());
//            Role role = roleRepository.findById((int)userRole.getRoleId());
//            BranchMaster branch = userRole.getBranch();
//            if (role != null)
//                auditTrial.setRole(role.getAuthority());
//
//            if (branch != null) {
//                auditTrial.setSol(branch.getSolId());
//                auditTrial.setBranchName(branch.getBranchName());
//            }
//        }
//        auditTrialRepository.save(auditTrial);
//
//        List<String> roles360 = userRoleService.getRoleOfUser(username);
//        List<String> delegatedRoles = userRoleService.getDelegatedRole(user);
//
//        Set<String> allRoles = new HashSet<>(roles360);
//        allRoles.addAll(delegatedRoles);
//
//        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
//
//
//        for (String role : allRoles){
//            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_360_" + role);
//            grantedAuthorities.add(grantedAuthority);
//        }
//
//
//
//        log.info("added " + grantedAuthorities +" authorities for user "+username);
//        return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return authentication.equals(UsernamePasswordAuthenticationToken.class);
//    }
//
//
//
//}