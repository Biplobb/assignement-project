package com.babl.Assignment.configs.notused;//package com.babl.citybank.configs;
//
//import com.babl.citybank.common.PasswordStatus;
//import com.babl.citybank.common.UserStatus;
//import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
//import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
//import com.babl.citybank.modules.adminPanel.services.UserRoleService;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.*;
//
//
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
//public class MyUserDetailsService implements UserDetailsService {
//    private final UserRoleService userRoleService;
//    private final UserRepository userRepository;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user = userRepository.findByUsername(username);
//
//        List<String> roles360 = userRoleService.getRoleOfUser(username);
//        List<String> delegatedRoles = userRoleService.getDelegatedRole(user);
//
//        Set<String> allRoles = new HashSet<>(roles360);
//        allRoles.addAll(delegatedRoles);
//
//        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
//
//
//        for (String role : allRoles){
//            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_360_" + role);
//            grantedAuthorities.add(grantedAuthority);
//        }
//
//        //user.setStatus(UserStatus.ACTIVE);
//        if (user.getLastLogin() != null) {
//            long daysBetweenLogin = ChronoUnit.DAYS.between(user.getLastLogin().toLocalDate(), LocalDateTime.now().toLocalDate());
//            if (daysBetweenLogin > 30)
//                user.setStatus(UserStatus.LOCKED);
//        }
//
//        user.setLastLogin(LocalDateTime.now());
//
//
//        //user.setPasswordStatus(PasswordStatus.VALID);
//        if (user.getLastPasswordChanged() != null) {
//            long passwordChangeDaysBetween = ChronoUnit.DAYS.between(user.getLastPasswordChanged().toLocalDate(), LocalDateTime.now().toLocalDate());
//            if (passwordChangeDaysBetween > 15)
//                user.setPasswordStatus(PasswordStatus.EXPIRED);
//        }
//
//
//        userRepository.save(user);
//
//        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), grantedAuthorities);
//    }
//}
