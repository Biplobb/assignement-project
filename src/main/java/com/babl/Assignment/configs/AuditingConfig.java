package com.babl.Assignment.configs;

import com.babl.Assignment.modules.login.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Configuration
@EnableJpaAuditing
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuditingConfig implements AuditorAware<String> {
    private final HttpServletRequest httpServletRequest;
    private final TokenService tokenService;

    @Override
    public Optional<String> getCurrentAuditor() {
        try {
            return tokenService.getName(httpServletRequest) == null ?
                    Optional.empty() : Optional.of(tokenService.getName(httpServletRequest));
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
