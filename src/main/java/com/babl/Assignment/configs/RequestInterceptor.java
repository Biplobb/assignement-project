package com.babl.Assignment.configs;

import com.babl.Assignment.exceptions.BadRequestException;
import com.babl.Assignment.modules.login.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RequestInterceptor implements HandlerInterceptor {
     private final TokenService tokenService;




}
