package com.babl.Assignment.configs;

import com.babl.Assignment.common.Constants;
import com.babl.Assignment.datasources.oracle.logins.BearerToken;
import com.babl.Assignment.datasources.oracle.logins.BearerTokenRepository;
import com.babl.Assignment.exceptions.CustomAuthenticationException;
import com.babl.Assignment.exceptions.UserAccessException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriTemplate;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BearerTokenValidationFilter implements Filter {
    private static final Pattern authorizationPattern = Pattern.compile("^Bearer (?<token>[a-zA-Z0-9-._~+/]+)=*$", 2);
    private static final Pattern requestPatternPattern = Pattern.compile("(?<token>[a-zA-Z0-9-._~+/]+)=*$");
    private BearerTokenRepository bearerTokenRepository;
    private AntPathMatcher pathMatcher;
    private UriTemplate rootFileGetUriTemplate;
    private UriTemplate tomcatFileGetUriTemplate;
    private UriTemplate rootFileGetByTypeUriTemplate;
    private UriTemplate tomcatFileGetByTypeUriTemplate;
    private UriTemplate rootSignatureGetTemplate;
    private UriTemplate tomcatSignatureGetTemplate;
    private UriTemplate rootIntgrSignatureTemplate;
    private UriTemplate tomcatIntgrSignatureTemplate;
    private UriTemplate rootIntgrPlatformSignatureTemplate;
    private UriTemplate tomcatIntgrPlatformSignatureTemplate;
    private UriTemplate rootIntgrMultiSignatureTemplate;
    private UriTemplate tomcatIntgrMultiSignatureTemplate;

    public BearerTokenValidationFilter(BearerTokenRepository bearerTokenRepository) {
        this.bearerTokenRepository = bearerTokenRepository;
        this.pathMatcher = new AntPathMatcher();

    }

    public static String resolveFromAuthorizationHeader(HttpServletRequest request) {
        if (request == null)
            return null;
        String authorization = request.getHeader("Authorization");
        if (StringUtils.startsWithIgnoreCase(authorization, "bearer")) {
            Matcher matcher = authorizationPattern.matcher(authorization);
            if (!matcher.matches()) {
                return null;
            } else {
                return matcher.group("token");
            }
        } else {
            return null;
        }
    }

    private boolean validateToken(String token, HttpServletResponse response) {
        if (token == null) {
            CustomAuthenticationException.handle(response, "Bearer Token Not Found or Invalid");
            return false;
        }

        token = token.trim();

        Optional<BearerToken> bearerTokenOptional = bearerTokenRepository.findById(token);
        if (!bearerTokenOptional.isPresent()) {
            try {
                byte[] key = Constants.tokenSecretKey.getBytes();
                Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
                String name = claims.getBody().getSubject();

                if (name == null || (!name.equals("rhuq94") && !name.equals("sysusr")))
                    throw new UserAccessException("Invalid permission");
                return true;
            } catch (Exception e) {
                CustomAuthenticationException.handle(response, "Invalid Bearer Token");
                return false;
            }
        }
        BearerToken bearerToken = bearerTokenOptional.get();

        LocalDateTime timeout = bearerToken.getTimeout();
        if (timeout.isBefore(LocalDateTime.now())) {
            CustomAuthenticationException.handle(response, "Bearer Token Timeout");
            return false;
        }

        bearerToken.setTimeout(LocalDateTime.now().plusMinutes(Constants.sessionTimeoutMinute));
        bearerTokenRepository.save(bearerToken);
        return true;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String uri = request.getRequestURI();
        if (uri.contains("/logMeIn") ) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else  {
            String token = resolveFromAuthorizationHeader(request);
            if (token == null)
                return;

            byte[] key = Constants.tokenSecretKey.getBytes();
            Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            String name = claims.getBody().getSubject();

            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private String resolveFromRequestParam(HttpServletRequest httpServletRequest) {
        Enumeration<String> paramNames = httpServletRequest.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            if (paramName.equalsIgnoreCase("access_token")) {
                String[] accessTokenArray = httpServletRequest.getParameterValues(paramName);
                if (accessTokenArray.length == 0)
                    return null;
                Matcher matcher = requestPatternPattern.matcher(accessTokenArray[0]);
                if (matcher.matches())
                    return accessTokenArray[0];
                return null;
            }
        }
        return null;
    }
}
