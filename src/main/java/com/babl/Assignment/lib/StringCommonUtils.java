package com.babl.Assignment.lib;


import org.springframework.stereotype.Component;

@Component
public class StringCommonUtils {

    public static String maskCardNumber(String cardNumber) {
        if (cardNumber == null)
            return null;
        int len = cardNumber.length();
        if (len < 12)
            return null;
        return cardNumber.substring(0, 6) + "******" + cardNumber.substring(12);

    }


    public static String maskCardNumberWithExtraStar(String cardNumber) {
        if (cardNumber == null)
            return null;
        int len = cardNumber.length();
        if (len < 12)
            return null;
        return cardNumber.substring(0, 6) + "******" + cardNumber.substring(11);

    }

    public static String maskCardNumber(Object cardNumber) {
        return maskCardNumber((String) cardNumber);
    }

    public static String getCardVariantByNo(String cardNo) {
        if (cardNo.startsWith("3"))
            return "AMEX";
        if (cardNo.startsWith("4"))
            return "VISA";
        if (cardNo.startsWith("5"))
            return "MASTER";
        return null;
    }
}
