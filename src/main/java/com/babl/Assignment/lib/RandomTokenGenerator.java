package com.babl.Assignment.lib;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RandomTokenGenerator {
    public String generate() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
