package com.babl.Assignment.lib;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatePickerFormatToLocaldate {
    public static LocalDate convert(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
        return LocalDate.parse(date, formatter);
    }


    public static String getOracleDefaultDateString(String inputDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yy");
        return DatePickerFormatToLocaldate.convert(inputDate).format(formatter);
    }
}
