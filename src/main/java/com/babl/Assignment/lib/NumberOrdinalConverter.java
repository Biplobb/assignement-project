package com.babl.Assignment.lib;

import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class NumberOrdinalConverter {
    private HashMap<Integer, String> hashMap = new HashMap();

    public NumberOrdinalConverter() {
        hashMap.put(1, "First");
        hashMap.put(2, "Second");
        hashMap.put(3, "Third");
        hashMap.put(4, "Fourth");
        hashMap.put(5, "Fifth");
        hashMap.put(6, "Sixth");
        hashMap.put(7, "Seventh");
        hashMap.put(8, "Eighth");
        hashMap.put(9, "Ninth");
        hashMap.put(10, "Tenth");
        hashMap.put(11, "Eleventh");
        hashMap.put(12, "Twelfth");
        hashMap.put(13, "Thirteenth");
        hashMap.put(14, "Fourteenth");
        hashMap.put(15, "Fifteenth");
        hashMap.put(16, "Sixteenth");
        hashMap.put(17, "Seventeenth");
        hashMap.put(18, "Eighteenth");
        hashMap.put(19, "Nineteenth");
        hashMap.put(20, "Twentieth");
        hashMap.put(21, "Twenty First");
        hashMap.put(22, "Twenty Second");
        hashMap.put(23, "Twenty Third");
        hashMap.put(24, "Twenty Fourth");
        hashMap.put(25, "Twenty Fifth");
        hashMap.put(26, "Twenty Sixth");
        hashMap.put(27, "Twenty Seventh");
        hashMap.put(28, "Twenty Eighth");
        hashMap.put(29, "Twenty Ninth");
    }

    public String convert(Integer number) {
        if (number < 1)
            return null;

        if (number < 30)
            return hashMap.get(number);
        return number + "th";
    }
}
